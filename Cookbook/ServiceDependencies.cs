﻿using Cookbook.Logger;
using Cookbook.Repositories;
using Cookbook.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Cookbook
{
    public static class ServiceDependencies
    {
        public static void Configure(IServiceCollection services)
        {
            services.AddScoped<INodeDboRepository, NodeDboRepository>();
            services.AddScoped<INodesService, NodesService>();
            services.AddScoped<DbContext, CookbookContext>();
            services.AddScoped<IRecipesChangesRepository, RecipesChangesRepository>();
            services.AddScoped<IRecipiesChangesService, RecipesChangesService>();
            services.AddSingleton(typeof(ICustomLogger), new CustomLogger().AddConsoleLog());
        }
    }
}
