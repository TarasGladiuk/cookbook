﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Cookbook.Controllers
{
    [ApiController]
    [Route("api/nodes")]
    public class NodesController : ControllerBase
    {
        private INodesService _nodesService;
        public NodesController(INodesService nodesService)
        {
            _nodesService = nodesService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var nodes = await _nodesService.GetNodesAsync();

            return Ok(nodes);
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody]Node node)
        {
            await _nodesService.UpdateAsync(node);

            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]Node node)
        {
            await _nodesService.AddAsync(node);

            return CreatedAtAction("Create", node);
        }
    }
}
