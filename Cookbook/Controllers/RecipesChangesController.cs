﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cookbook.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Cookbook.Controllers
{
    [ApiController]
    [Route("api/recipes")]
    public class RecipesChangesController : ControllerBase
    {
        private IRecipiesChangesService _recipiesChangesService;

        public RecipesChangesController(IRecipiesChangesService recipiesChangesService)
        {
            _recipiesChangesService = recipiesChangesService;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get([FromRoute] string id)
        {
            var recipesChanges = await _recipiesChangesService.GetAsync(Guid.Parse(id));

            return Ok(recipesChanges);
        }
    }
}
