﻿using Cookbook.Contexts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Logger
{
    public interface ICustomLogger
    {
        ICustomLogger AddConsoleLog();
        ICustomLogger AddDatabaseLog(LogsContext logsContext);
        ICustomLogger AddFileLog(string path);

        void Log(string message, LogLevel level);
    }
}
