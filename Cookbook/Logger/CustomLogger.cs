﻿using Cookbook.Contexts;
using Cookbook.Logger.LogMethods;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace Cookbook.Logger
{
    public class CustomLogger : ICustomLogger
    {
        private ISet<ILogMethod> _logMethods;

        public CustomLogger()
        {
            _logMethods = new HashSet<ILogMethod>();
        }

        public ICustomLogger AddConsoleLog()
        {
            _logMethods.Add(new ConsoleLog());

            return this;
        }

        public ICustomLogger AddDatabaseLog(LogsContext logsContext)
        {
            _logMethods.Add(new DatabaseLog(logsContext));

            return this;
        }

        public ICustomLogger AddFileLog(string path)
        {
            _logMethods.Add(new FileLog(path));

            return this;
        }

        public void Log(string message, LogLevel level)
        {
            if (_logMethods.Count > 0)
            {
                foreach (var method in _logMethods)
                {
                    method.Log(message, level);
                }
            }
        }
    }
}
