﻿using System;
using Cookbook.Contexts;
using Cookbook.Models;
using Microsoft.Extensions.Logging;

namespace Cookbook.Logger.LogMethods
{
    public class DatabaseLog : ILogMethod
    {
        private LogsContext _logsContext;
        
        public DatabaseLog(LogsContext logsContext)
        {
            _logsContext = logsContext;
        }

        public async void Log(string message, LogLevel level)
        {
            await _logsContext.Logs.AddAsync(new Log 
            {
                Date = DateTime.Now, 
                Level = level.ToString(), 
                Message = message 
            });
            await _logsContext.SaveChangesAsync();
        }
    }
}
