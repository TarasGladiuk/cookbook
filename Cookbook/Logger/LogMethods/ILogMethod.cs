﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Logger.LogMethods
{
    public interface ILogMethod
    {
        void Log(string message, LogLevel level); 
    }
}
