﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Cookbook.Logger.LogMethods
{
    public class FileLog : ILogMethod
    {
        private readonly string _logPath;

        public FileLog(string path)
        {
            _logPath = path;
        }

        public async void Log(string message, LogLevel level)
        {
            await File.AppendAllTextAsync(_logPath, $"{level.ToString()}: {message}.");
        }
    }
}
