﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cookbook.Utilities;
using Microsoft.Extensions.Logging;

namespace Cookbook.Logger.LogMethods
{
    public class ConsoleLog : ILogMethod
    {
        public void Log(string message, LogLevel level)
        {
            AsyncConsole.WriteLine($"{level.ToString()}: {message}.");        
        }
    }
}
