﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Cookbook.Middlewares
{
    public class ExceptionHandler
    {
        private readonly RequestDelegate _delegate;

        public ExceptionHandler(RequestDelegate del)
        {
            _delegate = del;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _delegate(context);
            }
            catch (Exception e)
            {
                await HandleExceptionAsync(context, e);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var response = context.Response;
            var message = exception.Message ?? "Unexpected error";
            var description = exception.StackTrace ?? "Unexpected error";
            response.ContentType = "application/json";
            response.StatusCode = (int)HttpStatusCode.InternalServerError;
            await response.WriteAsync(JsonConvert.SerializeObject(new 
            {
                Message = message,
                Description = description
            }));
        }
    }
}
