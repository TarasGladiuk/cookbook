import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl, NestedTreeControl } from '@angular/cdk/tree';
import { Component, Injectable, OnInit, ChangeDetectorRef } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { BehaviorSubject, Observable, of as observableOf } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ItemNode } from '../models/ItemNode';
import { DataService } from '../data/data.service';
import { MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material';
import { CreateRecipeComponent } from '../create-recipe/create-recipe.component';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { RecipeChanges } from '../models/RecipeChanges';
import { Attribute } from '@angular/compiler/src/core';

@Component({
    selector: 'app-home',
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.css'],
    providers: [DataService, CreateRecipeComponent]
})
export class HomeComponent {
    treeControl = new NestedTreeControl<ItemNode>(node => node.childrenNodes);
    dataSource = new MatTreeNestedDataSource<ItemNode>();
    dataChange = new BehaviorSubject<ItemNode[]>([]);
    data: ItemNode[];
    dataService: DataService;
    currentNode: ItemNode;
    newNode: ItemNode;
    recipeForm: FormGroup;
    recipeChanges: RecipeChanges[];

    constructor(http: HttpClient, public dialog: MatDialog, private fb: FormBuilder) {
        this.dataService = new DataService(http);
        this.dataService.getTreeData().subscribe(d => this.dataSource.data = d);
        this.recipeForm = this.fb.group({
            name: this.setName(),
            recipe: this.setRecipe()
        });
        this.recipeForm.valueChanges.subscribe(v => this.newNode = (v as ItemNode));
        this.recipeForm.disable();
        this.recipeChanges = [{ oldRecipe: "This is a sample. Please, choose any available recipe.", dateChanged: new Date() } as RecipeChanges];
    }

    hasChild = (_: number, nodeData: ItemNode) => { return !!nodeData.childrenNodes && nodeData.childrenNodes.length > 0; };
    hasNoContent = (_: number, nodeData: ItemNode) => { return !nodeData.name || nodeData.name.length == 0 };

    addNewItem(node: ItemNode) {
        this.openCreateRecipeDialog(node);
        this.dataService.getTreeData().subscribe(d => this.dataSource.data = d);
    }

    openCreateRecipeDialog(node: ItemNode) {
        const dialogRef = this.dialog.open(CreateRecipeComponent);
        dialogRef.afterClosed().subscribe(n => {
            this.dataService.insertItem(node, n.name, n.recipe).subscribe(r => {
                if ((r as Response).status == 201) {
                    console.log("success!")
                }
            });
        });
    }

    setForm(node: ItemNode) {
        this.recipeForm.patchValue({ name: node.name, recipe: node.recipe });
        this.currentNode = node;
    }

    updateForm(node: ItemNode) {
        this.setForm(node);
        this.recipeForm.enable();
        document.getElementById("submitButton").attributes.removeNamedItem("hidden");
        document.getElementById("closeEditingButton").attributes.removeNamedItem("hidden");
    }

    onSaveClick() {
        console.log("start")
        this.currentNode.name = this.newNode.name;
        this.currentNode.recipe = this.newNode.recipe;
        console.log(this.currentNode);
        this.dataService.updateItem(this.currentNode).subscribe(r => console.log(r));
        this.onCloseEditingClick();
        window.location.reload();
    }

    getRecipeChanges(id: string) {
        return this.dataService.getOldRecipes(id).subscribe(r => this.recipeChanges = r as RecipeChanges[]);
    }

    getFirstTwentySymbols(str: string) {
        if (str.length>20) {
            return str.slice(0, 19);
        }
        return str;
    }

    onCloseEditingClick() {
        this.recipeForm.disable()
        document.getElementById("submitButton").attributes.setNamedItem(document.createAttribute("hidden"));
        document.getElementById("closeEditingButton").attributes.setNamedItem(document.createAttribute("hidden"));
    }

    private setName(): string {
        return "Please, click on any available recipe.";
    }

    private setRecipe(): string {
        return "...";
    }
}
