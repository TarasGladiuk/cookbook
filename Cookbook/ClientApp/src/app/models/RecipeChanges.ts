export interface RecipeChanges {
    id: string,
    nodeID: string,
    oldRecipe: string;
    dateChanged: Date;
    node: object;
}
