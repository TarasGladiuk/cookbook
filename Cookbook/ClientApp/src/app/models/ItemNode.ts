export interface ItemNode {
    id?: string;
    recipe?: string;
    name?: string;
    dateCreated?: Date;
    parentNodeID?: string;
    childrenNodes?: ItemNode[]
  
}
