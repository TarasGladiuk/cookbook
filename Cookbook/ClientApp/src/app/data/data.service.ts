import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl, NestedTreeControl } from '@angular/cdk/tree';
import { Component, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpEventType, HttpHandler } from '@angular/common/http';
import { ItemNode } from '../models/ItemNode'

@Injectable()
export class DataService {
    httpClient: HttpClient;
    constructor(http: HttpClient) {
        this.httpClient = http;
    }

    getTreeData() {
        return this.httpClient.get<ItemNode[]>("https://localhost:5001/api/nodes");
    }

    insertItem(parent: ItemNode, name: string, recipe: string) {
        if (parent.childrenNodes) {
            return this.httpClient.post("https://localhost:5001/api/nodes",
                { ParentNodeID: parent.id, Name: name, Recipe: recipe } as ItemNode);
        }
    }

    updateItem(node: ItemNode) {
        return this.httpClient.put("https://localhost:5001/api/nodes", node);
    }

    getOldRecipes(id: string) {
        return this.httpClient.get('https://localhost:5001/api/recipes/' + id);
    }
}
