import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ItemNode } from '../models/ItemNode';

@Component({
    selector: 'create-recipe',
    templateUrl: 'create-recipe.component.html',
    styles:['create-recipe.component.css']
})
export class CreateRecipeComponent {
    name: string = "";
    recipe: string = "";
    constructor(
        public dialogRef: MatDialogRef<CreateRecipeComponent>) { }

    onSaveClick(): void {
        console.log(this.name, this.recipe);
        this.dialogRef.close({ name: this.name, recipe: this.recipe } as ItemNode);
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
