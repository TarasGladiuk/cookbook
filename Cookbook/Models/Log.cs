﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    public class Log
    {
        public Guid ID { get; set; }

        public string Message { get; set; }

        public string Level { get; set; }

        public DateTime Date { get; set; }
    }
}
