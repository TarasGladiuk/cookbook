﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    public class RecipeChanges
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        public Guid NodeID { get; set; }

        [Required]
        public string OldRecipe { get; set; }

        [Required]
        public DateTime DateChanged { get; set; }

        public NodeDbo Node { get; set; }
    }
}
