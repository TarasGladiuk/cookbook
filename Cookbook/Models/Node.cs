﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook
{
    public class Node
    {
        public Guid ID { get; set; }

        public string Recipe { get; set; }

        public IEnumerable<Node> ChildrenNodes { get; set; }

        public Guid? ParentNodeID { get; set; }

        public string Name { get; set; }

        public DateTime DateCreated { get; set; }

    }
}
