﻿using Cookbook.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cookbook
{
    public class NodeDbo
    {
        [Key]
        public Guid ID { get; set; }

        [MinLength(10)]
        public string Recipe { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        public Guid? ParentNodeID { get; set; }

        public ICollection<RecipeChanges> RecipesChanges { get; set; }
    }
}
