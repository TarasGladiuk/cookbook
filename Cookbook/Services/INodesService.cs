﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cookbook
{
    public interface INodesService
    {
        IEnumerable<Node> GetChildrenNodes(Node node);
        Task<IEnumerable<Node>> GetNodesAsync();
        Task UpdateAsync(Node node);
        Task AddAsync(Node node);
    }
}