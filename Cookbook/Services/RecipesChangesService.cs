﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cookbook.Models;
using Cookbook.Repositories;

namespace Cookbook.Services
{
    public class RecipesChangesService : IRecipiesChangesService
    {
        private readonly IRecipesChangesRepository _recipiesChangesRepository;

        public RecipesChangesService(IRecipesChangesRepository recipiesChangesRepository)
        {
            _recipiesChangesRepository = recipiesChangesRepository;
        }

        public async Task<IEnumerable<RecipeChanges>> GetAsync(Guid id)
        {
            return await _recipiesChangesRepository.GetAsync(id);
        }
    }
}
