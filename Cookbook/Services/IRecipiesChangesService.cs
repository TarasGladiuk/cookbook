﻿using Cookbook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Services
{
    public interface IRecipiesChangesService
    {
        Task<IEnumerable<RecipeChanges>> GetAsync(Guid id);
    }
}
