﻿using Cookbook.Models;
using Cookbook.Repositories;
using Cookbook.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook
{
    public class NodesService : INodesService
    {
        private readonly INodeDboRepository _nodeDboRepository;
        private readonly IRecipesChangesRepository _recipesChangesRepository;

        public NodesService(INodeDboRepository nodeDboRepository, IRecipesChangesRepository recipesChangesRepository)
        {
            _nodeDboRepository = nodeDboRepository;
            _recipesChangesRepository = recipesChangesRepository;
        }

        public async Task AddAsync(Node node)
        {
            Guard.NotNullOrEmpty(node, nameof(node));
            Guard.NotNullOrEmpty(node.Name, nameof(node.Name));
            Guard.NotNullOrEmpty(node.Recipe, nameof(node.Recipe));
            node.ID = Guid.NewGuid();
            node.DateCreated = DateTime.Now;
            await _nodeDboRepository.AddAsync(node.AsNodeDbo());
        }

        public IEnumerable<Node> GetChildrenNodes(Node node)
        {
            Guard.NotNullOrEmpty(node, nameof(node));
            var childrenNodes = _nodeDboRepository.GetNodesQueryable().Where(n => n.ParentNodeID == node.ID).ToNodeCollection();

            if (childrenNodes != null)
            {
                foreach (var childrenNode in childrenNodes)
                {
                    childrenNode.ChildrenNodes = GetChildrenNodes(childrenNode).ToList();
                }
            }

            return childrenNodes.OrderBy(cn => cn.Name);
        }

        public async Task<IEnumerable<Node>> GetNodesAsync()
        {
            var nodes = (await _nodeDboRepository.GetRootNodesAsync()).ToNodeCollection();

            foreach (var node in nodes)
            {
                node.ChildrenNodes = GetChildrenNodes(node);
            }

            return nodes.OrderBy(n => n.Name);
        }

        public async Task UpdateAsync(Node node)
        {
            Guard.NotNullOrEmpty(node, nameof(node));
            Guard.NotNullOrEmpty(node.ID, nameof(node.ID));
            Guard.NotNullOrEmpty(node.Name, nameof(node.Name));
            Guard.NotNullOrEmpty(node.Recipe, nameof(node.Recipe));
            var oldNode = await _nodeDboRepository.FindAsNoTrackingAsync(node.ID);
            Guard.NotNullOrEmpty(oldNode, "Node to update");

            if (!string.Equals(oldNode.Recipe, node.Recipe))
            {
                await _recipesChangesRepository.AddAsync(
                    new RecipeChanges { NodeID = node.ID, OldRecipe = oldNode.Recipe, DateChanged = DateTime.Now });
            }

            await _nodeDboRepository.UpdateAsync(node.AsNodeDbo());
        }
    }
}
