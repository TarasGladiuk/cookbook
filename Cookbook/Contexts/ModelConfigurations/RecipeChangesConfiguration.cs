﻿using Cookbook.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Contexts.ModelConfigurations
{
    public class RecipeChangesConfiguration : IEntityTypeConfiguration<RecipeChanges>
    {
        public void Configure(EntityTypeBuilder<RecipeChanges> builder)
        {
            builder.Property(r => r.ID).ValueGeneratedOnAdd();
        }
    }
}
