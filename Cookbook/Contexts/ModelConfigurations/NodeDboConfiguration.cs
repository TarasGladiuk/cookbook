﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Cookbook.Contexts.ModelConfigurations
{
    public class NodeDboConfiguration : IEntityTypeConfiguration<NodeDbo>
    {
        public void Configure(EntityTypeBuilder<NodeDbo> builder)
        {
            builder.Property(n => n.Name).IsRequired();
            builder.HasMany(n => n.RecipesChanges)
                .WithOne(r => r.Node)
                .OnDelete(DeleteBehavior.Cascade);
            InitializeData(builder);
        }

        private void InitializeData(EntityTypeBuilder<NodeDbo> builder)
        {
            var guid = Guid.NewGuid();
            builder.HasData(
                new NodeDbo
                {
                    ID = guid,
                    DateCreated = DateTime.Now,
                    ParentNodeID = null,
                    Name = "Fried Meat",
                    Recipe = "Just fry some meat."
                },
                new NodeDbo
                {
                    ID = Guid.NewGuid(),
                    DateCreated = DateTime.Now,
                    ParentNodeID = guid,
                    Name = "Fried meat with pepper",
                    Recipe = "Just fry some meat with papper."
                },
                new NodeDbo
                {
                    ID = Guid.NewGuid(),
                    DateCreated = DateTime.Now,
                    ParentNodeID = guid,
                    Name = "Fried meat with salt",
                    Recipe = "Just fry some meat with salt."
                });
        }
    }
}
