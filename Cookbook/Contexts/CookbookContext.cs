﻿using Cookbook.Contexts.ModelConfigurations;
using Cookbook.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook
{
    public class CookbookContext : DbContext
    {
        public CookbookContext(DbContextOptions<CookbookContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new NodeDboConfiguration());
            modelBuilder.ApplyConfiguration(new RecipeChangesConfiguration());
        }

        public DbSet<NodeDbo> Nodes { get; set; }

        public DbSet<RecipeChanges> RecipeChanges { get; set; }
    }
}
