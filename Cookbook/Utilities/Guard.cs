﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Utilities
{
    public class Guard
    {
        public static void NotNullOrEmpty<T>(T item, string message = null)
        {
            var exception = new ArgumentNullException($"{message ?? nameof(item)} cannot be null.");

            _ = item ?? throw exception;

            if (item is string)
            {
                if (string.IsNullOrEmpty(item.ToString().Trim()))
                {
                    throw exception;
                }
            }
        }
    }
}
