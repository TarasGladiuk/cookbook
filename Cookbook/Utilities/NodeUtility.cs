﻿using Cookbook.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook
{
    public static class NodeUtility
    {
        public static IEnumerable<Node> ToNodeCollection(this IEnumerable<NodeDbo> nodes)
        {
            Guard.NotNullOrEmpty(nodes, nameof(nodes));

            return nodes
                .Select(n => new Node
                {
                    ID = n.ID,
                    ParentNodeID = n.ParentNodeID,
                    Recipe = n.Recipe,
                    Name = n.Name,
                    DateCreated = n.DateCreated
                })
                .ToList();
        }

        public static NodeDbo AsNodeDbo(this Node node)
        {
            Guard.NotNullOrEmpty(node, nameof(node));

            return new NodeDbo
            {
                ID = node.ID,
                ParentNodeID = node.ParentNodeID,
                Recipe = node.Recipe,
                Name = node.Name,
                DateCreated = node.DateCreated
            };
        }
    }
}
