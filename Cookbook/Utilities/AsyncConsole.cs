﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cookbook.Utilities
{
    public static class AsyncConsole
    {
        private static ConcurrentQueue<string> _messageQueue = new ConcurrentQueue<string>();

        static AsyncConsole()
        {
            var thread = new Thread(
                () =>
                {
                    while (true)
                    {
                        string message;
                        if (_messageQueue.TryDequeue(out message))
                        {
                            Console.WriteLine(message);
                        }
                    }
                });

            thread.IsBackground = true;
            thread.Start();
        }

        public static void WriteLine(string message)
        {
            _messageQueue.Enqueue(message);
        }
    }
}
