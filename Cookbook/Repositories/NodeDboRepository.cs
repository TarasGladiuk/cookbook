﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook
{
    public class NodeDboRepository : INodeDboRepository
    {
        private DbContext _context { get; set; }
        public NodeDboRepository(DbContext context)
        {
            _context = context;
        }

        public async Task<List<NodeDbo>> GetRootNodesAsync()
        {
            return await _context.Set<NodeDbo>().Where(n => n.ParentNodeID == null).ToListAsync();
        }

        public IQueryable<NodeDbo> GetNodesQueryable()
        {
            return _context.Set<NodeDbo>();
        }

        public async Task UpdateAsync(NodeDbo nodeDbo)
        {
            if (!await ExistsAsync(nodeDbo.ID))
            {
                throw new ArgumentNullException("Such node doesn't exist!");
            }

            _context.Set<NodeDbo>().Update(nodeDbo);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> ExistsAsync(Guid id)
        {
            return await _context.Set<NodeDbo>().AnyAsync(n => n.ID == id);
        }

        public async Task AddAsync(NodeDbo nodeDbo)
        {
            if (nodeDbo.ParentNodeID.HasValue)
            {
                if (!(await ExistsAsync(nodeDbo.ParentNodeID.Value)))
                {
                    throw new ArgumentException("Such parent node doesn't exist!");
                }
            }

            await _context.Set<NodeDbo>().AddAsync(nodeDbo);
            await _context.SaveChangesAsync();
        }

        public async Task<NodeDbo> FindAsNoTrackingAsync(Guid id)
        {
            return await _context.Set<NodeDbo>().AsNoTracking().Where(n => n.ID == id).FirstOrDefaultAsync();
        }
    }
}
