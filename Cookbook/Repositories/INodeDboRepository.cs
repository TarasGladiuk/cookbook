﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook
{
    public interface INodeDboRepository
    {
        Task<List<NodeDbo>> GetRootNodesAsync();
        IQueryable<NodeDbo> GetNodesQueryable();
        Task UpdateAsync(NodeDbo nodeDbo);
        Task<bool> ExistsAsync(Guid id);
        Task AddAsync(NodeDbo nodeDbo);

        Task<NodeDbo> FindAsNoTrackingAsync(Guid id);
    }
}
