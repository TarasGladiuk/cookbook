﻿using Cookbook.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Repositories
{
    public class RecipesChangesRepository : IRecipesChangesRepository
    {
        private DbContext _context;

        public RecipesChangesRepository(DbContext context)
        {
            _context = context;
        }

        public async Task AddAsync(RecipeChanges changes)
        {
            await _context.Set<RecipeChanges>().AddAsync(changes);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<RecipeChanges>> GetAsync(Guid id)
        {
            return await _context.Set<RecipeChanges>().Where(r=>r.NodeID==id).ToListAsync();
        }
    }
}
