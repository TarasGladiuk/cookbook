﻿using Cookbook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Repositories
{
    public interface IRecipesChangesRepository
    {
        Task AddAsync(RecipeChanges changes);
        Task<IEnumerable<RecipeChanges>> GetAsync(Guid id);
    }
}
