﻿using AutoFixture;
using Cookbook.Controllers;
using Cookbook.Logger;
using Cookbook.Repositories;
using Cookbook.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Xunit;

namespace Cookbook.Tests.IntegrationTests
{
    public class NodesControllerTest : IDisposable
    {
        private DbContext _context;
        private IServiceProvider _serviceProvider;

        public NodesControllerTest()
        {
            _serviceProvider = SetRequiredServices();
            _context = SetDbContext();
        }

        [Fact]
        public async void Get_ReturnsOk()
        {
            var fixture = new Fixture();
            var guid = Guid.NewGuid();
            var nodesDbo = new List<NodeDbo>
            {
                fixture.Build<NodeDbo>().With(n=>n.ID, guid).Without(n=>n.RecipesChanges).Without(n=>n.ParentNodeID).Create(),
                fixture.Build<NodeDbo>().With(n=>n.ParentNodeID, guid).Without(n=>n.RecipesChanges).Create(),
                fixture.Build<NodeDbo>().With(n=>n.ParentNodeID, guid).Without(n=>n.RecipesChanges).Create(),
                fixture.Build<NodeDbo>().With(n=>n.ParentNodeID, guid).Without(n=>n.RecipesChanges).Create(),
                fixture.Build<NodeDbo>().With(n=>n.ParentNodeID, guid).Without(n=>n.RecipesChanges).Create(),
                fixture.Build<NodeDbo>().With(n=>n.ParentNodeID, guid).Without(n=>n.RecipesChanges).Create()
            };
            _context.Set<NodeDbo>().AddRange(nodesDbo);
            _context.SaveChanges();

            var nodesController = new NodesController((INodesService)_serviceProvider.GetService(typeof(INodesService)));

            var result = await nodesController.Get() as OkObjectResult;

            Assert.NotNull(result);
            Assert.True(result.StatusCode == (int)HttpStatusCode.OK);
            var nodes = (IOrderedEnumerable<Node>)result.Value;
            Assert.NotNull(nodes);
            Assert.Contains(nodes, n => n.ID == guid);
            Assert.NotNull(nodes.FirstOrDefault(n => n.ID.ToString() == guid.ToString()).ChildrenNodes);
            Assert.True(nodes.FirstOrDefault(n => n.ID.ToString() == guid.ToString()).ChildrenNodes.Count() == 5);
        }

        [Fact]
        public async void Create_ValidModel_ReturnsCreatedAtAction()
        {
            var fixture = new Fixture();
            var node = fixture.Build<Node>().Without(n => n.ParentNodeID).Without(n => n.ChildrenNodes).Create();

            var nodesController = new NodesController((INodesService)_serviceProvider.GetService(typeof(INodesService)));

            var result = await nodesController.Create(node) as CreatedAtActionResult;

            Assert.NotNull(_context);
            Assert.NotNull(result);
            Assert.True(_context.Set<NodeDbo>().Any(n => n.ID == node.ID));
            Assert.True(result.StatusCode == (int)HttpStatusCode.Created);
        }

        [Fact]
        public async void Create_InvalidModel_ThrowsArgumentNullException()
        {
            var node = new Node();

            var nodesController = new NodesController((INodesService)_serviceProvider.GetService(typeof(INodesService)));

            var result = await Assert.ThrowsAsync<ArgumentNullException>(async () => await nodesController.Create(node));
            Assert.NotNull(result);
            Assert.Contains("Name cannot be null.", result.Message);
        }

        [Fact]
        public async void Update_ValidModel_ReturnsNoContent()
        {
            var fixture = new Fixture();
            var guid = Guid.NewGuid();
            var nodeBefore = fixture.Build<NodeDbo>().With(n => n.ID, guid).Without(n => n.RecipesChanges).Without(n => n.ParentNodeID).Create();
            await _context.Set<NodeDbo>().AddAsync(nodeBefore);
            await _context.SaveChangesAsync();
            var nodeAfter = fixture.Build<Node>().With(n => n.ID, guid).Without(n => n.ChildrenNodes).Without(n => n.ParentNodeID).Create();

            var nodesController = new NodesController((INodesService)_serviceProvider.GetService(typeof(INodesService)));

            var result = await nodesController.Update(nodeAfter) as NoContentResult;

            Assert.NotNull(result);
            Assert.True(result.StatusCode == (int)HttpStatusCode.NoContent);
            
        }

        [Fact]
        public async void Update_InvalidModel_ThrowsArgumentNullException()
        {
            var fixture = new Fixture();
            var guid = Guid.NewGuid();
            var node = fixture.Build<Node>().With(n => n.ID, guid).Without(n => n.ChildrenNodes).Without(n => n.ParentNodeID).Create();

            var nodesController = new NodesController((INodesService)_serviceProvider.GetService(typeof(INodesService)));

            var result = await Assert.ThrowsAsync<ArgumentNullException>(async () => await nodesController.Update(node));

            Assert.NotNull(result);
            Assert.Contains("Node to update cannot be null.", result.Message);
        }
        public void Dispose()
        {
            _context.Database.EnsureDeleted();
        }

        private IServiceProvider SetRequiredServices()
        { 
            return new ServiceCollection()
                .AddEntityFrameworkSqlServer()
                .AddScoped<INodeDboRepository, NodeDboRepository>()
                .AddScoped<INodesService, NodesService>()
                .AddScoped<IRecipesChangesRepository, RecipesChangesRepository>()
                .AddScoped<IRecipiesChangesService, RecipesChangesService>()
                .AddDbContext<CookbookContext>(o => o.UseInMemoryDatabase("CookbookTest"))
                .AddScoped<DbContext, CookbookContext>()
                .AddSingleton(typeof(ICustomLogger), new CustomLogger())
                .BuildServiceProvider();
        }

        private DbContext SetDbContext()
        {
            var builder = new DbContextOptionsBuilder<CookbookContext>();

            builder.UseInMemoryDatabase("CookbookTest");

            return new CookbookContext(builder.Options);
        }
    }
}
