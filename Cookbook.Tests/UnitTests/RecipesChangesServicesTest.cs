﻿using Cookbook.Models;
using Cookbook.Repositories;
using Cookbook.Services;
using Moq;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Cookbook.Tests
{
    public class RecipesChangesServicesTest
    {
        private readonly Mock<IRecipesChangesRepository> _recipiesChangesRepository;
        public RecipesChangesServicesTest()
        {
            _recipiesChangesRepository = new Mock<IRecipesChangesRepository>();
        }

        [Fact]
        public async void GetAsync_ReturnsListOfRecipesChanges()
        {
            var id = new Guid("11111111-1111-1111-1111-111111111111");
            var list = new List<RecipeChanges> { new RecipeChanges {NodeID = id }, new RecipeChanges {NodeID = id } };

            _recipiesChangesRepository.Setup(n => n.GetAsync(It.IsAny<Guid>())).ReturnsAsync(list);

            var recipesChangesService = new RecipesChangesService(_recipiesChangesRepository.Object);

            var result = await recipesChangesService.GetAsync(id);

            Assert.NotNull(result);
            Assert.True(result.Count() == 2);
            Assert.True(result.All(r => r.NodeID == id));
            _recipiesChangesRepository.Verify(r => r.GetAsync(It.IsAny<Guid>()), Times.Once);
            _recipiesChangesRepository.VerifyNoOtherCalls();
        }
    }
}
