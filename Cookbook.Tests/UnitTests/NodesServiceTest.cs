using AutoFixture;
using Cookbook.Models;
using Cookbook.Repositories;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Cookbook.Tests
{
    public class NodesServiceTest
    {
        private Mock<INodeDboRepository> _nodeDboRepository;
        private Mock<IRecipesChangesRepository> _recipesChangesRepository;

        public NodesServiceTest()
        {
            _nodeDboRepository = new Mock<INodeDboRepository>();
            _recipesChangesRepository = new Mock<IRecipesChangesRepository>();
        }

        [Fact]
        public async void AddAsync_ReturnsCompletedTask()
        {
            var fixture = new Fixture();
            var node = fixture.Build<Node>().Without(n => n.ChildrenNodes).Without(n => n.ParentNodeID).Create();

            _nodeDboRepository.Reset();
            _nodeDboRepository.Setup(n => n.AddAsync(It.IsAny<NodeDbo>())).Returns(Task.CompletedTask);

            var nodesService = new NodesService(_nodeDboRepository.Object, _recipesChangesRepository.Object);

            await Assert.IsAssignableFrom<Task>(nodesService.AddAsync(node));
            _nodeDboRepository.Verify(n => n.AddAsync(It.IsAny<NodeDbo>()), Times.Once());
            _nodeDboRepository.VerifyNoOtherCalls();
        }

        [Fact]
        public void GetChildrenNodes_ReturnsChildrenNodes()
        {
            var id = new Guid("11111111-1111-1111-1111-111111111111");
            var childrenNodes = new List<NodeDbo> { new NodeDbo { ParentNodeID = id } };
            var node = new Node { ID = id };

            _nodeDboRepository.Reset();
            _nodeDboRepository.Setup(n => n.GetNodesQueryable()).Returns(childrenNodes.AsQueryable());

            var nodesService = new NodesService(_nodeDboRepository.Object, _recipesChangesRepository.Object);

            var result = nodesService.GetChildrenNodes(node);

            Assert.NotNull(result);
            Assert.NotEmpty(result);
            Assert.True(result.Count() == 1);
            _nodeDboRepository.Verify(n => n.GetNodesQueryable(), Times.Exactly(2));
            _nodeDboRepository.VerifyNoOtherCalls();
        }

        [Fact]
        public async void GetNodesAsync_ReturnsInstantiatedNodes()
        {
            var id = new Guid("11111111-1111-1111-1111-111111111111");
            var node = new NodeDbo { ParentNodeID = id };
            var rootNode = new NodeDbo { ID = id };
            var rootNodes = new List<NodeDbo> { rootNode };
            var childrenNodes = new List<NodeDbo> { node };

            _nodeDboRepository.Reset();
            _nodeDboRepository.Setup(n => n.GetRootNodesAsync()).ReturnsAsync(rootNodes);
            _nodeDboRepository.Setup(n => n.GetNodesQueryable()).Returns(childrenNodes.AsQueryable());

            var nodesService = new NodesService(_nodeDboRepository.Object, _recipesChangesRepository.Object);

            var result = await nodesService.GetNodesAsync();

            Assert.NotNull(result);
            Assert.True(result.Count() == 1);
            Assert.NotNull(result.FirstOrDefault().ChildrenNodes);
            Assert.True(result.FirstOrDefault().ChildrenNodes.Count() == 1);
            _nodeDboRepository.Verify(n => n.GetRootNodesAsync(), Times.Once);
            _nodeDboRepository.Verify(n => n.GetNodesQueryable(), Times.Exactly(2));
            _nodeDboRepository.VerifyNoOtherCalls();
        }

        [Fact]
        public async void UpdateAsync_ReturnsCompletedTask()
        {
            var fixture = new Fixture();
            var id = new Guid("11111111-1111-1111-1111-111111111111");
            var nodeDbo = fixture
                .Build<NodeDbo>()
                .Without(n => n.ParentNodeID)
                .Without(n => n.RecipesChanges)
                .With(n => n.Recipe, string.Empty)
                .With(n => n.ID, id)
                .Create();
            var node = fixture
                .Build<Node>()
                .Without(n => n.ParentNodeID)
                .Without(n => n.ChildrenNodes)
                .With(n => n.Recipe, "recipe")
                .With(n => n.ID, id)
                .Create();

            _nodeDboRepository.Reset();
            _recipesChangesRepository.Reset();
            _nodeDboRepository.Setup(n => n.FindAsNoTrackingAsync(It.IsAny<Guid>())).ReturnsAsync(nodeDbo);
            _recipesChangesRepository.Setup(n => n.AddAsync(It.IsAny<RecipeChanges>())).Returns(Task.CompletedTask);
            _nodeDboRepository.Setup(n => n.UpdateAsync(It.IsAny<NodeDbo>())).Returns(Task.CompletedTask);

            var nodesService = new NodesService(_nodeDboRepository.Object, _recipesChangesRepository.Object);

            await Assert.IsAssignableFrom<Task>(nodesService.UpdateAsync(node));
            _nodeDboRepository.Verify(n => n.FindAsNoTrackingAsync(It.IsAny<Guid>()), Times.Once);
            _nodeDboRepository.Verify(n => n.UpdateAsync(It.IsAny<NodeDbo>()), Times.Once());
            _recipesChangesRepository.Verify(r => r.AddAsync(It.IsAny<RecipeChanges>()), Times.Once);
            _nodeDboRepository.VerifyNoOtherCalls();
            _recipesChangesRepository.VerifyNoOtherCalls();
        }
    }
}
